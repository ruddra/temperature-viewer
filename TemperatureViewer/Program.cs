﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TemperatureViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ManagementObjectSearcher searcher =
                      new ManagementObjectSearcher(
                          @"root\WMI", "SELECT * FROM MSAcpi_ThermalZoneTemperature"
                      );
            foreach (ManagementObject item in searcher.Get())
            {

                foreach (ManagementObject obj in searcher.Get())
                {
                    Double temp = Convert.ToDouble(obj["CurrentTemperature"].ToString());
                    temp = (temp - 2732) / 10.0;
                    string temperature = Convert.ToString(temp);
                    MessageBox.Show(string.Format(" Your CPU temperature is {0}*C!!!", temperature), "Results", MessageBoxButtons.OK);
                }
            }
        }
    }
}
